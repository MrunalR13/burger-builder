import React from 'react';
import Aux from '../../hoc/Aux';
import Style from './Layout.module.css';

const Layout = props => (
  <Aux>
    <div>Toolbar, SideDrawer , BackDrop</div>
    <main className={Style.Content}>{props.children}</main>
  </Aux>
);

export default Layout;
