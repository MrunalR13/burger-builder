import React from 'react';
import Style from './BuildControls.module.css';
import BuildControl from './BuildControl/BuildControl';

const controls =[
    { label:'Salad' ,type:'salad'},
    {label:'Bacon',type:'bacon'},
    {label:'Cheese',type:'cheese'},
    {label:'Meat',type:'meat'},
];


const buildControls = (props) => {
    return (  

        <div className={Style.BuildControls}>
        <p>Total Price :<strong>{props.price}</strong></p>
        {controls.map(ctrl => (
            <BuildControl 
            key={ctrl.label} 
            label={ctrl.label}
            added={() => props.ingredientAdded(ctrl.type)}
            removed={()=>props.ingredientRemoved(ctrl.type)}
            disabled={props.disabled[ctrl.type]}/>
        ))}

        <div className={Style.OrderButton}
        
        disabled={!props.purchasable}><strong>CHECKOUT</strong></div>

        </div> 
    );
}
 
export default buildControls;