import React, { Component } from 'react';
import Aux from '../../hoc/Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
//import buildControls from '../../components/Burger/BuildControls/BuildControls';

const INGREDENT_PRICES ={
      salad:10,
      meat:20,
      bacon:10,
      cheese:10

}

  class BurgerBuilder extends Component {
    state={
      ingredients:{
      salad:0,
      meat:0,
      bacon:0,
      cheese:0
      },
      totalPrice: 100,
      purcjasable:false

    }

    updatePurchaseState(ingredients){
      
      const sum =Object.keys(ingredients)
      .map(igKey =>{
        return ingredients[igKey];
      })
      .reduce((sum,el)=>{
        return sum+el;
      },0);
      this.setState({purchasable :sum>0});
    }

    addIngredientHandler =(type)=>{
      const oldCount =this.state.ingredients[type];
      const updatedCount = oldCount+1;
      const updatedIngredients ={
        ...this.state.ingredients   
       };
       updatedIngredients[type] =updatedCount;
       const priceAddition = INGREDENT_PRICES[type];
       const oldPrice =this.state.totalPrice;
       const newPrice=oldPrice+priceAddition;
       this.setState({totalPrice: newPrice,ingredients:updatedIngredients});
       this.updatePurchaseState(updatedIngredients);
       
    }

    removeIngredientHandler =(type)=>{
      const oldCount =this.state.ingredients[type];
      if(oldCount <=0)
      {
        return;
      }
      const updatedCount = oldCount-1;
      const updatedIngredients ={
        ...this.state.ingredients   
       };
       updatedIngredients[type] =updatedCount;
       const priceDeduction = INGREDENT_PRICES[type];
       const oldPrice =this.state.totalPrice;
       const newPrice=oldPrice-priceDeduction;
       this.setState({totalPrice: newPrice,ingredients:updatedIngredients});
       this.updatePurchaseState(updatedIngredients);
    }
    render() {

      const disabledInfo ={
        ...this.state.ingredients
      };

      for(let key in disabledInfo){
        disabledInfo[key]=disabledInfo[key] <=0
      }
      return (
        <Aux>
          <Burger ingredients={this.state.ingredients}/>
         <BuildControls
         ingredientAdded={this.addIngredientHandler}
         ingredientRemoved={this.removeIngredientHandler}
         disabled={disabledInfo} 
         purchasable={this.state.purchasable}
         price={this.state.totalPrice}/>
        </Aux>
      );
    }

  }

export default BurgerBuilder;